package Codility;

public class CyclicRotation {
    public static void main(String[] args) {
        int[] array = {4, 9, 3, 7};

        for (int i : solution(array, 3)) {
            System.out.print(i + " ");
        }
    }

    private static int[] solution(int[] inputArray, int times) {
        for (int i = 0; i < times; i++) {
            inputArray = rotateArray(inputArray);
        }

        return inputArray;
    }

    private static int[] rotateArray(int[] inputArray) {
        int[] outputArray = new int[inputArray.length];
        for (int i = 0; i < inputArray.length; i++) {
            if (i + 1 == inputArray.length) {
                outputArray[0] = inputArray[i];
            } else {
                outputArray[i + 1] = inputArray[i];
            }
        }

        return outputArray;
    }
}
