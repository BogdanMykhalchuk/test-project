package Codility;

public class BinaryGap {
    public static void main(String[] args) {
        System.out.println(solution(249673085));
    }

    private static Integer solution(int n) {
        String binaryRep = Integer.toBinaryString(n);
        char[] chars = binaryRep.toCharArray();
        int counter = 0;
        int gap = 0;
        boolean started = false;
        for (char ch : chars) {
            switch(ch) {

                case '0' : if (started) counter++; break;

                case '1':
                    if (started) {
                        if (counter > gap) {
                            gap = counter;
                        }
                        counter = 0;
                    } else {
                        started = true;
                    }
            }
        }

        return gap;
    }

}
