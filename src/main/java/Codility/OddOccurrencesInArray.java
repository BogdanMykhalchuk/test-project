package Codility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.util.Objects.isNull;

public class OddOccurrencesInArray {
    public static void main(String[] args) {
        System.out.println(solution(prepareArrayWithPairedInts(1000000, 1000000000)));
    }

    private static Integer solution(int[] A) {
        Map<Integer, Integer> valuesAmount = new HashMap<>();
        Integer value;
        for (int i : A) {
            value = valuesAmount.get(i);
            if (isNull(value)) {
                valuesAmount.put(i, 1);
            } else {
                valuesAmount.put(i, valuesAmount.get(i) + 1);
            }
        }

        for (Map.Entry<Integer, Integer> mapEntry : valuesAmount.entrySet()) {
            if (mapEntry.getValue() % 2 != 0) return mapEntry.getKey();
        }

        throw new IllegalArgumentException("Please pass array with valid state");
    }

    private static int[] prepareArrayWithPairedInts(int maxSize, int maxValue) {
        int size;

        Random r = new Random();
        while (true) {
            int i = r.nextInt(maxSize);
            if (i % 2 == 0) {
                size = i;
                break;
            }
        }

        Integer[] array = new Integer[size/2];

        for (int i = 0; i < size/2; i++) {
            array[i] = r.nextInt(maxValue);
        }

        List<Integer> list = new ArrayList<>();
        list.addAll(Arrays.asList(array));
        list.addAll(Arrays.asList(array));

        Collections.shuffle(list);

        while (true) {
            int withoutPair = r.nextInt(maxValue);
            if (!list.contains(withoutPair)) {
                list.add(withoutPair);
                break;
            }
        }

        int[] preparedArray = new int[list.size()];

        for (int i = 0; i < list.size(); i++) {
            preparedArray[i] = list.get(i);
        }

        return preparedArray;
    }
}
