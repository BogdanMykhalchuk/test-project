package Codility;

public class MaxCounters {
    public static void main(String[] args) {
        int[] array = {3, 4, 4, 6, 1, 4, 4};
        for (int i : solution(5, array)) {
            System.out.println(i);
        }
    }

    private static int[] solution(int N, int[] A) {
        int max = 0;
        int min = 0;
        int temp;

        int[] counters = new int[N];

        for (int i = 0; i < A.length; i++) {
            temp = A[i];

            if (temp >= 1 && temp <= N) {
                counters[temp - 1] = Math.max(counters[temp - 1], min) + 1;
                max = Math.max(max, counters[temp - 1]);
            } else if (temp == N + 1) {
                min = max;
            }
        }

        for (int i = 0; i < counters.length; i++) {
            counters[i] = Math.max(counters[i], min);
        }

        return counters;
    }

    // insufficiently effective method
    private static int[] solution1(int N, int[] A) {
        int[] counters = new int[N];
        int maxCounter = 0;

        for (int i = 0; i < A.length; i++) {
            if (A[i] == N + 1) {
                setAllValuesTo(counters, maxCounter);
            }

            if (A[i] >= 1 && A[i] <= N) {
                int j = A[i] - 1;
                counters[j] = counters[j] + 1;
                if (counters[j] > maxCounter) {
                    maxCounter = counters[j];
                }
            }
        }

        return counters;
    }

    private static void setAllValuesTo(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            array[i] = value;
        }
    }
}
