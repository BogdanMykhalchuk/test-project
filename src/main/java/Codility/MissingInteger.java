package Codility;

import java.util.HashMap;
import java.util.Map;

public class MissingInteger {
    public static void main(String[] args) {
        int[] array1 = {1, 3, 6, 4, 1, 2};
        int[] array2 = {1, 2, 3};
        int[] array3 = {-1, -3};
        System.out.println(solution(array1));
        System.out.println(solution(array2));
        System.out.println(solution(array3));
    }

    private static int solution(int[] A) {
        Map<Integer, Boolean> map = new HashMap<>(A.length);
        for (int i = 0; i < A.length; i++) {
            map.put(i + 1, false);
        }

        for (int i = 0; i < A.length; i++) {
            map.put(A[i], true);
        }

        int min = A.length + 1;
        for (Map.Entry<Integer, Boolean> entry : map.entrySet()) {
            if (!entry.getValue()) {
                if (min > entry.getKey()) {
                    min = entry.getKey();
                }
            }
        }

        return min;
    }
}
