package Codility;

import java.math.BigDecimal;

public class FrogJmp {
    public static void main(String[] args) {
        System.out.println(solution2(1, 1, 30));
    }

    // not efficient
    private static int solution(int X, int Y, int D) {
        int totalLength = Y - X;
        int passedPath = 0;
        int counter = 0;

        while (true) {
            if (passedPath >= totalLength) return counter;
            counter++;
            passedPath = passedPath + D;
        }
    }

    // O(1) algorithm complexity
    private static int solution2(int X, int Y, int D) {
        double counter = (Y - X) * 1.0 / D;
        if (counter % 1 == 0) return (int) counter;
        return (int) ++counter;
    }
}
