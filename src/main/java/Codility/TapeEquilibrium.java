package Codility;

public class TapeEquilibrium {
    public static void main(String[] args) {
        int[] array = {3, 1, 2, 4, 3};
        System.out.println(solution(array));
    }

    private static int solution(int[] A) {
        int leftSum = A[0];
        int rightSum = 0;

        for (int i = 1; i < A.length; i++) {
            rightSum = rightSum + A[i];
        }

        int min = Math.abs(leftSum - rightSum);
        int result;

        for (int i = 1; i < A.length - 1; i++) {
            leftSum = leftSum + A[i];
            rightSum = rightSum - A[i];
            result = Math.abs(leftSum - rightSum);
            if (min > result) {
                min = result;
            }
        }

        return min;
    }
}
