package Codility;

import java.util.HashMap;
import java.util.Map;

public class PermCheck {
    public static void main(String[] args) {
        int[] array = {4, 1, 3, 2};
        System.out.println(solution(array));
    }

    private static int solution(int[] A) {
        Map<Integer, Boolean> map = new HashMap<>(A.length);

        for (int i = 0; i < A.length; i++) {
            map.put(i+1, false);
        }

        for (int i = 0; i < A.length; i++) {
            map.put(A[i], true);
        }

        for (Boolean value : map.values()) {
            if (!value) {
                return 0;
            }
        }

        return 1;
    }
}
