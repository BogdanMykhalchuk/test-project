package UFIREG.UfiregMeteo;

import UFIREG.UFIREG_Daily.TheJobIsDone;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MeteoCalculator {
    private final static DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    static void startCalculations(String filePath) {
        String firstPartOfOutputFilePath = filePath.substring(0, filePath.lastIndexOf('\\') + 1);
        String secondPartOfOutputFilePath = filePath.substring(filePath.lastIndexOf('\\'),
                filePath.length() - 11);
        String outputFilePath = firstPartOfOutputFilePath + secondPartOfOutputFilePath + ".csv";
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(outputFilePath));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePath))) {

            calculation(bufferedReader, bufferedWriter);

        } catch (IOException e) {
            e.printStackTrace();
        }
        TheJobIsDone.printCongratulation();
    }


    private static void calculation(BufferedReader bufferedReader, BufferedWriter bufferedWriter) throws IOException {
        writeToFile(bufferedWriter, readFromFile(bufferedReader));
        bufferedReader.close();
    }


    private static List<MeteoDto> readFromFile(BufferedReader bufferedReader) throws IOException {
        List<MeteoDto> dtos = new ArrayList<>();
        
        // todo finish this method
        populateMeteoWithLags(dtos);
        return dtos;
    }

    private static double average(List<Double> list) {
        Double j = 0.0;
        for(Double object : list) {
            j = j + object;
        }
        j = j / list.size();
        return j;
    }

    private static void writeToFile(BufferedWriter bufferedWriter, List<MeteoDto> list) {
        try {
            for(MeteoDto object : list) {
                bufferedWriter.append(object.toString()).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private static void populateMeteoWithLags(List<MeteoDto> dtos) {
        for (int i = 0; i < dtos.size(); i++) {
            if ( i == 0) {
                continue;
            }
            if (i == 1) {
                populateWithLag1(i, dtos);
            }
            if (i == 2) {
                populateWithLag2(i, dtos);
            }
        }
    }
    
    private static void populateWithLag1(int i, List<MeteoDto> dtos) {
        dtos.get(i).setHumidityLag_1(dtos.get(i - 1).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_1(dtos.get(i - 1).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_1(dtos.get(i - 1).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_1(dtos.get(i - 1).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_1(dtos.get(i - 1).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_1(dtos.get(i - 1).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_1(dtos.get(i - 1).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_1(dtos.get(i - 1).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_1(dtos.get(i - 1).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_1(dtos.get(i - 1).getTempLag_0());
        dtos.get(i).setTempMaxLag_1(dtos.get(i - 1).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_1(dtos.get(i - 1).getTempMinLag_0());
    }

    private static void populateWithLag2(int i, List<MeteoDto> dtos) {
        populateWithLag1(i, dtos);
        
        dtos.get(i).setHumidityLag_2(dtos.get(i - 2).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_2(dtos.get(i - 2).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_2(dtos.get(i - 2).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_2(dtos.get(i - 2).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_2(dtos.get(i - 2).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_2(dtos.get(i - 2).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_2(dtos.get(i - 2).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_2(dtos.get(i - 2).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_2(dtos.get(i - 2).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_2(dtos.get(i - 2).getTempLag_0());
        dtos.get(i).setTempMaxLag_2(dtos.get(i - 2).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_2(dtos.get(i - 2).getTempMinLag_0());
    }

    private static void populateWithLag3(int i, List<MeteoDto> dtos) {
        populateWithLag2(i, dtos);

        dtos.get(i).setHumidityLag_3(dtos.get(i - 3).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_3(dtos.get(i - 3).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_3(dtos.get(i - 3).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_3(dtos.get(i - 3).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_3(dtos.get(i - 3).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_3(dtos.get(i - 3).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_3(dtos.get(i - 3).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_3(dtos.get(i - 3).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_3(dtos.get(i - 3).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_3(dtos.get(i - 3).getTempLag_0());
        dtos.get(i).setTempMaxLag_3(dtos.get(i - 3).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_3(dtos.get(i - 3).getTempMinLag_0());
    }

    private static void populateWithLag4(int i, List<MeteoDto> dtos) {
        populateWithLag3(i, dtos);

        dtos.get(i).setHumidityLag_4(dtos.get(i - 4).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_4(dtos.get(i - 4).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_4(dtos.get(i - 4).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_4(dtos.get(i - 4).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_4(dtos.get(i - 4).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_4(dtos.get(i - 4).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_4(dtos.get(i - 4).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_4(dtos.get(i - 4).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_4(dtos.get(i - 4).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_4(dtos.get(i - 4).getTempLag_0());
        dtos.get(i).setTempMaxLag_4(dtos.get(i - 4).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_4(dtos.get(i - 4).getTempMinLag_0());
    }

    private static void populateWithLag5(int i, List<MeteoDto> dtos) {
        populateWithLag4(i, dtos);

        dtos.get(i).setHumidityLag_5(dtos.get(i - 5).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_5(dtos.get(i - 5).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_5(dtos.get(i - 5).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_5(dtos.get(i - 5).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_5(dtos.get(i - 5).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_5(dtos.get(i - 5).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_5(dtos.get(i - 5).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_5(dtos.get(i - 5).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_5(dtos.get(i - 5).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_5(dtos.get(i - 5).getTempLag_0());
        dtos.get(i).setTempMaxLag_5(dtos.get(i - 5).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_5(dtos.get(i - 5).getTempMinLag_0());
    }

    private static void populateWithLag6(int i, List<MeteoDto> dtos) {
        populateWithLag5(i, dtos);

        dtos.get(i).setHumidityLag_6(dtos.get(i - 6).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_6(dtos.get(i - 6).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_6(dtos.get(i - 6).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_6(dtos.get(i - 6).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_6(dtos.get(i - 6).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_6(dtos.get(i - 6).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_6(dtos.get(i - 6).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_6(dtos.get(i - 6).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_6(dtos.get(i - 6).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_6(dtos.get(i - 6).getTempLag_0());
        dtos.get(i).setTempMaxLag_6(dtos.get(i - 6).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_6(dtos.get(i - 6).getTempMinLag_0());
    }

    private static void populateWithLag7(int i, List<MeteoDto> dtos) {
        populateWithLag6(i, dtos);

        dtos.get(i).setHumidityLag_7(dtos.get(i - 7).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_7(dtos.get(i - 7).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_7(dtos.get(i - 7).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_7(dtos.get(i - 7).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_7(dtos.get(i - 7).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_7(dtos.get(i - 7).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_7(dtos.get(i - 7).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_7(dtos.get(i - 7).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_7(dtos.get(i - 7).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_7(dtos.get(i - 7).getTempLag_0());
        dtos.get(i).setTempMaxLag_7(dtos.get(i - 7).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_7(dtos.get(i - 7).getTempMinLag_0());
    }

    private static void populateWithLag8(int i, List<MeteoDto> dtos) {
        populateWithLag7(i, dtos);

        dtos.get(i).setHumidityLag_8(dtos.get(i - 8).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_8(dtos.get(i - 8).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_8(dtos.get(i - 8).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_8(dtos.get(i - 8).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_8(dtos.get(i - 8).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_8(dtos.get(i - 8).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_8(dtos.get(i - 8).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_8(dtos.get(i - 8).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_8(dtos.get(i - 8).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_8(dtos.get(i - 8).getTempLag_0());
        dtos.get(i).setTempMaxLag_8(dtos.get(i - 8).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_8(dtos.get(i - 8).getTempMinLag_0());
    }

    private static void populateWithLag9(int i, List<MeteoDto> dtos) {
        populateWithLag8(i, dtos);

        dtos.get(i).setHumidityLag_9(dtos.get(i - 9).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_9(dtos.get(i - 9).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_9(dtos.get(i - 9).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_9(dtos.get(i - 9).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_9(dtos.get(i - 9).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_9(dtos.get(i - 9).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_9(dtos.get(i - 9).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_9(dtos.get(i - 9).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_9(dtos.get(i - 9).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_9(dtos.get(i - 9).getTempLag_0());
        dtos.get(i).setTempMaxLag_9(dtos.get(i - 9).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_9(dtos.get(i - 9).getTempMinLag_0());
    }

    private static void populateWithLag10(int i, List<MeteoDto> dtos) {
        populateWithLag9(i, dtos);

        dtos.get(i).setHumidityLag_10(dtos.get(i - 10).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_10(dtos.get(i - 10).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_10(dtos.get(i - 10).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_10(dtos.get(i - 10).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_10(dtos.get(i - 10).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_10(dtos.get(i - 10).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_10(dtos.get(i - 10).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_10(dtos.get(i - 10).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_10(dtos.get(i - 10).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_10(dtos.get(i - 10).getTempLag_0());
        dtos.get(i).setTempMaxLag_10(dtos.get(i - 10).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_10(dtos.get(i - 10).getTempMinLag_0());
    }

    private static void populateWithLag11(int i, List<MeteoDto> dtos) {
        populateWithLag10(i, dtos);

        dtos.get(i).setHumidityLag_11(dtos.get(i - 11).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_11(dtos.get(i - 11).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_11(dtos.get(i - 11).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_11(dtos.get(i - 11).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_11(dtos.get(i - 11).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_11(dtos.get(i - 11).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_11(dtos.get(i - 11).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_11(dtos.get(i - 11).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_11(dtos.get(i - 11).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_11(dtos.get(i - 11).getTempLag_0());
        dtos.get(i).setTempMaxLag_11(dtos.get(i - 11).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_11(dtos.get(i - 11).getTempMinLag_0());
    }

    private static void populateWithLag12(int i, List<MeteoDto> dtos) {
        populateWithLag11(i, dtos);

        dtos.get(i).setHumidityLag_12(dtos.get(i - 12).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_12(dtos.get(i - 12).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_12(dtos.get(i - 12).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_12(dtos.get(i - 12).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_12(dtos.get(i - 12).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_12(dtos.get(i - 12).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_12(dtos.get(i - 12).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_12(dtos.get(i - 12).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_12(dtos.get(i - 12).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_12(dtos.get(i - 12).getTempLag_0());
        dtos.get(i).setTempMaxLag_12(dtos.get(i - 12).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_12(dtos.get(i - 12).getTempMinLag_0());
    }

    private static void populateWithLag13(int i, List<MeteoDto> dtos) {
        populateWithLag12(i, dtos);

        dtos.get(i).setHumidityLag_13(dtos.get(i - 13).getHumidityLag_0());
        dtos.get(i).setHumidityMaxLag_13(dtos.get(i - 13).getHumidityMaxLag_0());
        dtos.get(i).setHumidityMinLag_13(dtos.get(i - 13).getHumidityMinLag_0());

        dtos.get(i).setPressureLag_13(dtos.get(i - 13).getPressureLag_0());
        dtos.get(i).setPressureMaxLag_13(dtos.get(i - 13).getPressureMaxLag_0());
        dtos.get(i).setPressureMinLag_13(dtos.get(i - 13).getPressureMinLag_0());

        dtos.get(i).setPressureStatLag_13(dtos.get(i - 13).getPressureStatLag_0());
        dtos.get(i).setPressureStatMaxLag_13(dtos.get(i - 13).getPressureStatMaxLag_0());
        dtos.get(i).setPressureStatMinLag_13(dtos.get(i - 13).getPressureStatMinLag_0());

        dtos.get(i).setTempLag_13(dtos.get(i - 13).getTempLag_0());
        dtos.get(i).setTempMaxLag_13(dtos.get(i - 13).getTempMaxLag_0());
        dtos.get(i).setTempMinLag_13(dtos.get(i - 13).getTempMinLag_0());
    }

    private static void populateWithLag0_1(int i, List<MeteoDto> dtos) {
        populateWithLag13(i, dtos);
        
        MeteoDto meteoDto = dtos.get(i);

        // todo care about null values

        meteoDto.setHumidityLag_0_1((meteoDto.getHumidityLag_0() + meteoDto.getHumidityLag_1()) / 2);
        meteoDto.setHumidityMaxLag_0_1((meteoDto.getHumidityMaxLag_0() + meteoDto.getHumidityMaxLag_1()) / 2);
        meteoDto.setHumidityMinLag_0_1((meteoDto.getHumidityMinLag_0() + meteoDto.getHumidityMinLag_1()) / 2);

        meteoDto.setPressureLag_0_1((meteoDto.getPressureLag_0() + meteoDto.getPressureLag_1()) / 2);
        meteoDto.setPressureMaxLag_0_1((meteoDto.getPressureMaxLag_0() + meteoDto.getPressureMaxLag_1()) / 2);
        meteoDto.setPressureMinLag_0_1((meteoDto.getPressureMinLag_0() + meteoDto.getPressureMinLag_1()) / 2);

        meteoDto.setPressureStatLag_0_1((meteoDto.getPressureStatLag_0() + meteoDto.getPressureStatLag_1()) / 2);
        meteoDto.setPressureStatMaxLag_0_1((meteoDto.getPressureStatMaxLag_0() + meteoDto.getPressureStatMaxLag_1()) / 2);
        meteoDto.setPressureStatMinLag_0_1((meteoDto.getPressureStatMinLag_0() + meteoDto.getPressureStatMinLag_1()) / 2);

        meteoDto.setTempLag_0_1((meteoDto.getTempLag_0() + meteoDto.getTempLag_1()) / 2);
        meteoDto.setTempMaxLag_0_1((meteoDto.getTempMaxLag_0() + meteoDto.getTempMaxLag_1()) / 2);
        meteoDto.setTempMinLag_0_1((meteoDto.getTempMinLag_0() + meteoDto.getTempMinLag_1()) / 2);
    }

    private static void populateWithLag2_5(int i, List<MeteoDto> dtos) {
        populateWithLag0_1(i, dtos);

        MeteoDto meteoDto = dtos.get(i);

        // todo care about null values

        meteoDto.setHumidityLag_2_5((meteoDto.getHumidityLag_2() + meteoDto.getHumidityLag_3() + meteoDto.getHumidityLag_4() + meteoDto.getHumidityLag_5()) / 4);
        meteoDto.setHumidityMaxLag_2_5((meteoDto.getHumidityMaxLag_2() + meteoDto.getHumidityMaxLag_3() + meteoDto.getHumidityMaxLag_4() + meteoDto.getHumidityMaxLag_5()) / 4);
        meteoDto.setHumidityMinLag_2_5((meteoDto.getHumidityMinLag_2() + meteoDto.getHumidityMinLag_3() + meteoDto.getHumidityMinLag_4() + meteoDto.getHumidityMinLag_5()) / 4);

        meteoDto.setPressureLag_2_5((meteoDto.getPressureLag_2() + meteoDto.getPressureLag_3() + meteoDto.getPressureLag_4() + meteoDto.getPressureLag_5()) / 4);
        meteoDto.setPressureMaxLag_2_5((meteoDto.getPressureMaxLag_2() + meteoDto.getPressureMaxLag_3() + meteoDto.getPressureMaxLag_4() + meteoDto.getPressureMaxLag_5()) / 4);
        meteoDto.setPressureMinLag_2_5((meteoDto.getPressureMinLag_2() + meteoDto.getPressureMinLag_3() + meteoDto.getPressureMinLag_4() + meteoDto.getPressureMinLag_5()) / 4);

        meteoDto.setPressureStatLag_2_5((meteoDto.getPressureStatLag_2() + meteoDto.getPressureStatLag_3() + meteoDto.getPressureStatLag_4() + meteoDto.getPressureStatLag_5()) / 4);
        meteoDto.setPressureStatMaxLag_2_5((meteoDto.getPressureStatMaxLag_2() + meteoDto.getPressureStatMaxLag_3() + meteoDto.getPressureStatMaxLag_4() + meteoDto.getPressureStatMaxLag_5()) / 4);
        meteoDto.setPressureStatMinLag_2_5((meteoDto.getPressureStatMinLag_2() + meteoDto.getPressureStatMinLag_3() + meteoDto.getPressureStatMinLag_4() + meteoDto.getPressureStatMinLag_5()) / 4);

        meteoDto.setTempLag_2_5((meteoDto.getTempLag_2() + meteoDto.getTempLag_3() + meteoDto.getTempLag_4() + meteoDto.getTempLag_5()) / 4);
        meteoDto.setTempMaxLag_2_5((meteoDto.getTempMaxLag_2() + meteoDto.getTempMaxLag_3() + meteoDto.getTempMaxLag_4() + meteoDto.getTempMaxLag_5()) / 4);
        meteoDto.setTempMinLag_2_5((meteoDto.getTempMinLag_2() + meteoDto.getTempMinLag_3() + meteoDto.getTempMinLag_4() + meteoDto.getTempMinLag_5()) / 4);
    }

    private static void populateWithLag0_5(int i, List<MeteoDto> dtos) {
        populateWithLag2_5(i, dtos);

        MeteoDto meteoDto = dtos.get(i);

        // todo care about null values

    }

    private static void populateWithLag2_13(int i, List<MeteoDto> dtos) {
        populateWithLag0_5(i, dtos);

        MeteoDto meteoDto = dtos.get(i);

        // todo care about null values

    }
}
