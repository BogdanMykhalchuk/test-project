package UFIREG.UfiregMeteo;

import lombok.Getter;
import lombok.Setter;

import java.util.Locale;

@Getter
@Setter
public class MeteoDto {
    // Humidity
    private Double humidityLag_0;
    private Double humidityLag_1;
    private Double humidityLag_2;
    private Double humidityLag_3;
    private Double humidityLag_4;
    private Double humidityLag_5;
    private Double humidityLag_6;
    private Double humidityLag_7;
    private Double humidityLag_8;
    private Double humidityLag_9;
    private Double humidityLag_10;
    private Double humidityLag_11;
    private Double humidityLag_12;
    private Double humidityLag_13;
    private Double humidityLag_0_1;
    private Double humidityLag_2_5;
    private Double humidityLag_0_5;
    private Double humidityLag_2_13;
    // Humidity Max
    private Double humidityMaxLag_0;
    private Double humidityMaxLag_1;
    private Double humidityMaxLag_2;
    private Double humidityMaxLag_3;
    private Double humidityMaxLag_4;
    private Double humidityMaxLag_5;
    private Double humidityMaxLag_6;
    private Double humidityMaxLag_7;
    private Double humidityMaxLag_8;
    private Double humidityMaxLag_9;
    private Double humidityMaxLag_10;
    private Double humidityMaxLag_11;
    private Double humidityMaxLag_12;
    private Double humidityMaxLag_13;
    private Double humidityMaxLag_0_1;
    private Double humidityMaxLag_2_5;
    private Double humidityMaxLag_0_5;
    private Double humidityMaxLag_2_13;
    // Humidity Min
    private Double humidityMinLag_0;
    private Double humidityMinLag_1;
    private Double humidityMinLag_2;
    private Double humidityMinLag_3;
    private Double humidityMinLag_4;
    private Double humidityMinLag_5;
    private Double humidityMinLag_6;
    private Double humidityMinLag_7;
    private Double humidityMinLag_8;
    private Double humidityMinLag_9;
    private Double humidityMinLag_10;
    private Double humidityMinLag_11;
    private Double humidityMinLag_12;
    private Double humidityMinLag_13;
    private Double humidityMinLag_0_1;
    private Double humidityMinLag_2_5;
    private Double humidityMinLag_0_5;
    private Double humidityMinLag_2_13;
    
    // Pressure
    private Double pressureLag_0;
    private Double pressureLag_1;
    private Double pressureLag_2;
    private Double pressureLag_3;
    private Double pressureLag_4;
    private Double pressureLag_5;
    private Double pressureLag_6;
    private Double pressureLag_7;
    private Double pressureLag_8;
    private Double pressureLag_9;
    private Double pressureLag_10;
    private Double pressureLag_11;
    private Double pressureLag_12;
    private Double pressureLag_13;
    private Double pressureLag_0_1;
    private Double pressureLag_2_5;
    private Double pressureLag_0_5;
    private Double pressureLag_2_13;
    // pressure Max
    private Double pressureMaxLag_0;
    private Double pressureMaxLag_1;
    private Double pressureMaxLag_2;
    private Double pressureMaxLag_3;
    private Double pressureMaxLag_4;
    private Double pressureMaxLag_5;
    private Double pressureMaxLag_6;
    private Double pressureMaxLag_7;
    private Double pressureMaxLag_8;
    private Double pressureMaxLag_9;
    private Double pressureMaxLag_10;
    private Double pressureMaxLag_11;
    private Double pressureMaxLag_12;
    private Double pressureMaxLag_13;
    private Double pressureMaxLag_0_1;
    private Double pressureMaxLag_2_5;
    private Double pressureMaxLag_0_5;
    private Double pressureMaxLag_2_13;
    // pressure Min
    private Double pressureMinLag_0;
    private Double pressureMinLag_1;
    private Double pressureMinLag_2;
    private Double pressureMinLag_3;
    private Double pressureMinLag_4;
    private Double pressureMinLag_5;
    private Double pressureMinLag_6;
    private Double pressureMinLag_7;
    private Double pressureMinLag_8;
    private Double pressureMinLag_9;
    private Double pressureMinLag_10;
    private Double pressureMinLag_11;
    private Double pressureMinLag_12;
    private Double pressureMinLag_13;
    private Double pressureMinLag_0_1;
    private Double pressureMinLag_2_5;
    private Double pressureMinLag_0_5;
    private Double pressureMinLag_2_13;

    // Pressure stat
    private Double pressureStatLag_0;
    private Double pressureStatLag_1;
    private Double pressureStatLag_2;
    private Double pressureStatLag_3;
    private Double pressureStatLag_4;
    private Double pressureStatLag_5;
    private Double pressureStatLag_6;
    private Double pressureStatLag_7;
    private Double pressureStatLag_8;
    private Double pressureStatLag_9;
    private Double pressureStatLag_10;
    private Double pressureStatLag_11;
    private Double pressureStatLag_12;
    private Double pressureStatLag_13;
    private Double pressureStatLag_0_1;
    private Double pressureStatLag_2_5;
    private Double pressureStatLag_0_5;
    private Double pressureStatLag_2_13;
    // pressure stat Max
    private Double pressureStatMaxLag_0;
    private Double pressureStatMaxLag_1;
    private Double pressureStatMaxLag_2;
    private Double pressureStatMaxLag_3;
    private Double pressureStatMaxLag_4;
    private Double pressureStatMaxLag_5;
    private Double pressureStatMaxLag_6;
    private Double pressureStatMaxLag_7;
    private Double pressureStatMaxLag_8;
    private Double pressureStatMaxLag_9;
    private Double pressureStatMaxLag_10;
    private Double pressureStatMaxLag_11;
    private Double pressureStatMaxLag_12;
    private Double pressureStatMaxLag_13;
    private Double pressureStatMaxLag_0_1;
    private Double pressureStatMaxLag_2_5;
    private Double pressureStatMaxLag_0_5;
    private Double pressureStatMaxLag_2_13;
    // pressure stat Min
    private Double pressureStatMinLag_0;
    private Double pressureStatMinLag_1;
    private Double pressureStatMinLag_2;
    private Double pressureStatMinLag_3;
    private Double pressureStatMinLag_4;
    private Double pressureStatMinLag_5;
    private Double pressureStatMinLag_6;
    private Double pressureStatMinLag_7;
    private Double pressureStatMinLag_8;
    private Double pressureStatMinLag_9;
    private Double pressureStatMinLag_10;
    private Double pressureStatMinLag_11;
    private Double pressureStatMinLag_12;
    private Double pressureStatMinLag_13;
    private Double pressureStatMinLag_0_1;
    private Double pressureStatMinLag_2_5;
    private Double pressureStatMinLag_0_5;
    private Double pressureStatMinLag_2_13;

    // Temperature
    private Double tempLag_0;
    private Double tempLag_1;
    private Double tempLag_2;
    private Double tempLag_3;
    private Double tempLag_4;
    private Double tempLag_5;
    private Double tempLag_6;
    private Double tempLag_7;
    private Double tempLag_8;
    private Double tempLag_9;
    private Double tempLag_10;
    private Double tempLag_11;
    private Double tempLag_12;
    private Double tempLag_13;
    private Double tempLag_0_1;
    private Double tempLag_2_5;
    private Double tempLag_0_5;
    private Double tempLag_2_13;
    // Temperature Max
    private Double tempMaxLag_0;
    private Double tempMaxLag_1;
    private Double tempMaxLag_2;
    private Double tempMaxLag_3;
    private Double tempMaxLag_4;
    private Double tempMaxLag_5;
    private Double tempMaxLag_6;
    private Double tempMaxLag_7;
    private Double tempMaxLag_8;
    private Double tempMaxLag_9;
    private Double tempMaxLag_10;
    private Double tempMaxLag_11;
    private Double tempMaxLag_12;
    private Double tempMaxLag_13;
    private Double tempMaxLag_0_1;
    private Double tempMaxLag_2_5;
    private Double tempMaxLag_0_5;
    private Double tempMaxLag_2_13;
    // Temperature Min
    private Double tempMinLag_0;
    private Double tempMinLag_1;
    private Double tempMinLag_2;
    private Double tempMinLag_3;
    private Double tempMinLag_4;
    private Double tempMinLag_5;
    private Double tempMinLag_6;
    private Double tempMinLag_7;
    private Double tempMinLag_8;
    private Double tempMinLag_9;
    private Double tempMinLag_10;
    private Double tempMinLag_11;
    private Double tempMinLag_12;
    private Double tempMinLag_13;
    private Double tempMinLag_0_1;
    private Double tempMinLag_2_5;
    private Double tempMinLag_0_5;
    private Double tempMinLag_2_13;

    // Wind direction
    private Double north;
    private Double northNorthEast;
    private Double northEast;
    private Double eastNorthEast;
    private Double east;
    private Double eastSouthEast;
    private Double southEast;
    private Double southSouthEast;
    private Double south;
    private Double southSouthWest;
    private Double southWest;
    private Double westSouthWest;
    private Double west;
    private Double westNorthWest;
    private Double northWest;
    private Double northNorthWest;

    // Wind speed
    private Double windSpeed;
    private Double windMaxSpeed;
    private Double windMinSpeed;

    @Override
    public String toString() {
        return String.format(Locale.ROOT, 
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;%.2f;" +
                "%.2f;%.2f;%.2f;%.2f;%.2f;", 
                humidityLag_0, humidityLag_1, humidityLag_2, humidityLag_3, humidityLag_4, humidityLag_5, humidityLag_6, humidityLag_7, humidityLag_8, humidityLag_9, humidityLag_10, humidityLag_11, humidityLag_12, humidityLag_13, humidityLag_0_1, humidityLag_2_5, humidityLag_0_5, humidityLag_2_13,
                humidityMaxLag_0, humidityMaxLag_1, humidityMaxLag_2, humidityMaxLag_3, humidityMaxLag_4, humidityMaxLag_5, humidityMaxLag_6, humidityMaxLag_7, humidityMaxLag_8, humidityMaxLag_9, humidityMaxLag_10, humidityMaxLag_11, humidityMaxLag_12, humidityMaxLag_13, humidityMaxLag_0_1, humidityMaxLag_2_5, humidityMaxLag_0_5, humidityMaxLag_2_13,
                humidityMinLag_0, humidityMinLag_1, humidityMinLag_2, humidityMinLag_3, humidityMinLag_4, humidityMinLag_5, humidityMinLag_6, humidityMinLag_7, humidityMinLag_8, humidityMinLag_9, humidityMinLag_10, humidityMinLag_11, humidityMinLag_12, humidityMinLag_13, humidityMinLag_0_1, humidityMinLag_2_5, humidityMinLag_0_5, humidityMinLag_2_13,
                pressureLag_0, pressureLag_1, pressureLag_2, pressureLag_3, pressureLag_4, pressureLag_5, pressureLag_6, pressureLag_7, pressureLag_8, pressureLag_9, pressureLag_10, pressureLag_11, pressureLag_12, pressureLag_13, pressureLag_0_1, pressureLag_2_5, pressureLag_0_5, pressureLag_2_13,
                pressureMaxLag_0, pressureMaxLag_1, pressureMaxLag_2, pressureMaxLag_3, pressureMaxLag_4, pressureMaxLag_5, pressureMaxLag_6, pressureMaxLag_7, pressureMaxLag_8, pressureMaxLag_9, pressureMaxLag_10, pressureMaxLag_11, pressureMaxLag_12, pressureMaxLag_13, pressureMaxLag_0_1, pressureMaxLag_2_5, pressureMaxLag_0_5, pressureMaxLag_2_13,
                pressureMinLag_0, pressureMinLag_1, pressureMinLag_2, pressureMinLag_3, pressureMinLag_4, pressureMinLag_5, pressureMinLag_6, pressureMinLag_7, pressureMinLag_8, pressureMinLag_9, pressureMinLag_10, pressureMinLag_11, pressureMinLag_12, pressureMinLag_13, pressureMinLag_0_1, pressureMinLag_2_5, pressureMinLag_0_5, pressureMinLag_2_13,
                pressureStatLag_0, pressureStatLag_1, pressureStatLag_2, pressureStatLag_3, pressureStatLag_4, pressureStatLag_5, pressureStatLag_6, pressureStatLag_7, pressureStatLag_8, pressureStatLag_9, pressureStatLag_10, pressureStatLag_11, pressureStatLag_12, pressureStatLag_13, pressureStatLag_0_1, pressureStatLag_2_5, pressureStatLag_0_5, pressureStatLag_2_13,
                pressureStatMaxLag_0, pressureStatMaxLag_1, pressureStatMaxLag_2, pressureStatMaxLag_3, pressureStatMaxLag_4, pressureStatMaxLag_5, pressureStatMaxLag_6, pressureStatMaxLag_7, pressureStatMaxLag_8, pressureStatMaxLag_9, pressureStatMaxLag_10, pressureStatMaxLag_11, pressureStatMaxLag_12, pressureStatMaxLag_13, pressureStatMaxLag_0_1, pressureStatMaxLag_2_5, pressureStatMaxLag_0_5, pressureStatMaxLag_2_13,
                pressureStatMinLag_0, pressureStatMinLag_1, pressureStatMinLag_2, pressureStatMinLag_3, pressureStatMinLag_4, pressureStatMinLag_5, pressureStatMinLag_6, pressureStatMinLag_7, pressureStatMinLag_8, pressureStatMinLag_9, pressureStatMinLag_10, pressureStatMinLag_11, pressureStatMinLag_12, pressureStatMinLag_13, pressureStatMinLag_0_1, pressureStatMinLag_2_5, pressureStatMinLag_0_5, pressureStatMinLag_2_13,
                tempLag_0, tempLag_1, tempLag_2, tempLag_3, tempLag_4, tempLag_5, tempLag_6, tempLag_7, tempLag_8, tempLag_9, tempLag_10, tempLag_11, tempLag_12, tempLag_13, tempLag_0_1, tempLag_2_5, tempLag_0_5, tempLag_2_13,
                tempMaxLag_0, tempMaxLag_1, tempMaxLag_2, tempMaxLag_3, tempMaxLag_4, tempMaxLag_5, tempMaxLag_6, tempMaxLag_7, tempMaxLag_8, tempMaxLag_9, tempMaxLag_10, tempMaxLag_11, tempMaxLag_12, tempMaxLag_13, tempMaxLag_0_1, tempMaxLag_2_5, tempMaxLag_0_5, tempMaxLag_2_13,
                tempMinLag_0, tempMinLag_1, tempMinLag_2, tempMinLag_3, tempMinLag_4, tempMinLag_5, tempMinLag_6, tempMinLag_7, tempMinLag_8, tempMinLag_9, tempMinLag_10, tempMinLag_11, tempMinLag_12, tempMinLag_13, tempMinLag_0_1, tempMinLag_2_5, tempMinLag_0_5, tempMinLag_2_13,
                north, northNorthEast, northEast, eastNorthEast, east, eastSouthEast, southEast, southSouthEast,
                south, southSouthWest, southWest, westSouthWest, west, westNorthWest, northWest, northNorthWest,
                windSpeed, windMaxSpeed, windMinSpeed);
    }
}