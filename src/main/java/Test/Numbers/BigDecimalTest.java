package Test.Numbers;

import java.math.BigDecimal;

public class BigDecimalTest {
    public static void main(String[] args) throws Exception {

        BigDecimal first = new BigDecimal("0.00");
        BigDecimal second = BigDecimal.ZERO;

        System.out.println(first.compareTo(second));
        System.out.println(first.equals(second));
    }
}
