package Test.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "payment")
@Getter
@Setter
public class Payment {
    @XmlAttribute
    private String id;
    @XmlElement
    private List<Property> properties;
}
