package Test.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "data")
@Getter
@Setter
public class Data {
    @XmlElement
    private String oper;
    @XmlElement
    private Integer wait;
    @XmlElement
    private Integer test;
    @XmlElement
    private Payment payment;
}
