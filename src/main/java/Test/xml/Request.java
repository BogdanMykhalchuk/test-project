package Test.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "request")
@Getter
@Setter
public class Request {
    @XmlAttribute
    private String version;
    @XmlElement
    private Merchant merchant;
    @XmlElement
    private Data data;
}
