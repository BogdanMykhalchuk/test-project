package Test.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "prop")
@Getter
@Setter
public class Property {
    @XmlAttribute
    private String name;
    @XmlAttribute
    private String value;
}
