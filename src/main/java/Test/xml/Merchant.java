package Test.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "merchant")
@Getter
@Setter
public class Merchant {
    @XmlElement
    private Integer id;
    @XmlElement
    private String signature;
}
