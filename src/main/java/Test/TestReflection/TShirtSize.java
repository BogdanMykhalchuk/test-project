package Test.TestReflection;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TShirtSize {
    private Long id;
    private boolean yxs = true;
    private boolean ys = true;
    private boolean ym = true;
    private boolean yl = true;
    private boolean xs = true;
    private boolean s = true;
    private boolean m = true;
    private boolean l = true;
    private boolean xl = true;
    private boolean xxl = true;
}
