package Test.TestReflection;

import java.lang.reflect.Field;

public class TestReflection {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        TShirtSize tShirtSize = new TShirtSize();
        Field[] fields = TShirtSize.class.getDeclaredFields();

        for (Field field : fields) {
            System.out.println(field.getName());
        }

        System.out.println(TShirtSize.class.getDeclaredField("yxs").getName());

        System.out.println(tShirtSize.isL());

        Field field = TShirtSize.class.getDeclaredField("l");
        if (!field.isAccessible()) {
            field.setAccessible(true);
        }

        boolean actualValue = field.getBoolean(tShirtSize);
        field.setBoolean(tShirtSize, !actualValue);

        System.out.println(tShirtSize.isL());
        Field field1 = TShirtSize.class.getDeclaredField("l");
        System.out.println(field1.isAccessible());
    }
}
