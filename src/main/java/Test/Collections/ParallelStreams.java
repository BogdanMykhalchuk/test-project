package Test.Collections;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class ParallelStreams {
    public static void main(String[] args) throws Exception {
        // filling Test.list with numbers

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10000000; i++) {
            list.add(i);
        }


        // One thread stream sum of numbers

        long start = System.currentTimeMillis();
        long result = list.stream().mapToLong(Integer::longValue).sum();
        long finish = System.currentTimeMillis();

        System.out.println(result);
        System.out.println(finish - start);


        // Multi threads parallel streams sum of numbers

        start = System.currentTimeMillis();
        result = list.parallelStream().mapToLong(Integer::longValue).sum();
        finish = System.currentTimeMillis();

        System.out.println(result);
        System.out.println(finish - start);


        // Multi thread sum with hands

        start = System.currentTimeMillis();
        int cors = 4;
        int part = list.size() / cors;
        int limit = 0;
        result = 0;

        List<FutureTask<Long>> tasks = new ArrayList<>();

        for (int i = 0; i < cors; i++) {
            if (i < cors - 1) {
                Callable<Long> counter = new Counter(list, limit, (i + 1) * part);
                tasks.add(new FutureTask<Long>(counter));
                Thread thread = new Thread(tasks.get(i));
                thread.run();
                limit += part;
            } else {
                Callable<Long> counter = new Counter(list, limit, list.size());
                tasks.add(new FutureTask<Long>(counter));
                Thread thread = new Thread(tasks.get(i));
                thread.run();
            }
        }

        for (FutureTask<Long> task : tasks) {
            result += task.get();
        }

        finish = System.currentTimeMillis();

        System.out.println(result);
        System.out.println(finish - start);
    }

    private static class Counter implements Callable<Long> {

        private List<Integer> list = new ArrayList<>();
        private int start;
        private int end;

        private Counter(List<Integer> list, int start, int end) {
            this.list = list;
            this.start = start;
            this.end = end;
        }

        @Override
        public Long call() throws Exception {
            long sum = 0;
            for (int i = start; i < end; i++) {
                sum += list.get(i);
            }
            return sum;
        }
    }
}
