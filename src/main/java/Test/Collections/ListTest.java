package Test.Collections;

import java.util.*;

public class ListTest {
    public static void main(String[] args) {
        ArrayList<String> alist=new ArrayList<String>();
        alist.add("Steve");
        alist.add("Tim");
        alist.add("Lucy");
        alist.add("Pat");
        alist.add("Steve");
        alist.add("Tom");
        alist.remove("Steve");
        System.out.println(alist);
        System.out.println("\n");

        Collections.sort(alist, new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.compareTo(t1);
            }
        });

        List<String> linkedList = new LinkedList<>();
        linkedList.add("One");
        linkedList.add("Two");
        linkedList.add("Three");
        linkedList.add("Four");
        linkedList.add("Five");
        linkedList.add("Six");
        linkedList.add("Seven");
        linkedList.add("Eight");

        alist.forEach(System.out :: println);
        System.out.println("\n");
        alist.stream().forEach(System.out :: println);
        System.out.println("\n");

        System.out.println(linkedList.get(3));
        System.out.println(((LinkedList) linkedList).getFirst());

        ArrayList<Student> arraylist = new ArrayList<Student>();
        arraylist.add(new Student(223, "Chaitanya", 26));
        arraylist.add(new Student(245, "Rahul", 24));
        arraylist.add(new Student(209, "Buba", 22));
        arraylist.add(new Student(220, "Duda", 18));
        arraylist.add(new Student(215, "Goro", 25));


        java.util.Collections.sort(arraylist);

        for(Student str: arraylist){
            System.out.println(str);
        }

        System.out.println("\n");

        List<Student> sublist1 = arraylist.subList(0, 2);

        for(Student str: sublist1){
            System.out.println(str);
        }

        System.out.println("\n");

        List<Student> sublist2 = new ArrayList<>(arraylist.subList(0, 2));

        for(Student str: sublist2){
            System.out.println(str);
        }

        arraylist.get(0).setRollno(999);
        System.out.println("\n");

        for(Student str: sublist1){
            System.out.println(str);
        }

        System.out.println("\n");


        for(Student str: sublist2){
            System.out.println(str);
        }

        ArrayList<Student> students = (ArrayList)arraylist.clone();

        students.get(0).setRollno(1000);

        System.out.println("\n");

        for(Student str: sublist1){
            System.out.println(str);
        }

        System.out.println("\n");


        for(Student str: sublist2){
            System.out.println(str);
        }

        Comparator<Student> comparator = Student::compareBlaBlaBla;

        String[] strings = new String[5];
        for (int i = 0; i < 5; i++) {
            strings[i] = "Test/String " + i;
        }

        List<String> stringList = Arrays.asList(strings);

        System.out.println("Now we are going to meet an UnsupportedOperationException");
        try {
            stringList.add("Test.String 5");
        } catch (UnsupportedOperationException e) {
            e.printStackTrace();
        }
    }
}
