package Test.Collections;

import java.util.*;

public class MapTest {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        String var0 = "abc";
        System.out.println("abc".hashCode());
        int var1 = var0.hashCode();
        System.out.println(var1 ^ var1 >>> 16);
        int index = var1 & (16 - 1);
        System.out.println(index);
        for (int i = 0; i < 50; i++) {
            String key = UUID.randomUUID().toString();
            System.out.println(key.hashCode() & (map.size() - 1));
            map.put(key, UUID.randomUUID().toString());
        }
        System.out.println(map.size());
        map.put("fjaj", "jfsjf");
        map.containsKey("abc");

        Map<String, String> stringMap = new Hashtable<>();
        stringMap.put("jfkaj", null);

        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("sfs", "jfa");
    }
}
