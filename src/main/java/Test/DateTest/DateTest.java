package Test.DateTest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static java.util.Objects.isNull;

public class DateTest {
    public static void main(String[] args) throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        List<Date> dates = new ArrayList<>();
        dates.add(null);
        dates.add(dateFormat.parse("02-03-2015"));
        dates.add(dateFormat.parse("18-03-2015"));
        dates.add(dateFormat.parse("10-03-2015"));
        dates.add(dateFormat.parse("02-03-2015"));
        dates.add(null);
        dates.add(dateFormat.parse("12-03-2015"));
        dates.add(dateFormat.parse("02-03-2015"));
        dates.add(dateFormat.parse("26-03-2015"));
        dates.add(null);
        dates.add(dateFormat.parse("02-03-2015"));

        dates.sort(createComparatorBasedOnDueDate(Order.DESC));

        dates.forEach(t -> {
            if (isNull(t)) {
                System.out.println("null");
            } else {
                System.out.println(t.toString());
            }
        });
    }

    private static Comparator<Date> createComparatorBasedOnDueDate(Order order) {
        if (order.name().equals("ASC")) {
            return ((date1, date2) -> {
                if (isNull(date1)) {
                    return isNull(date2) ? 0 : 1;
                }

                if (isNull(date2)) {
                    return -1;
                }
                return date1.compareTo(date2);
            });
        } else {
            return ((date1, date2) -> {
                if (isNull(date2)) {
                    return isNull(date1) ? 0 : -1;
                }
                if (isNull(date1)) {
                    return 1;
                }
                return date2.compareTo(date1);
            });
        }
    }
    
    private static enum Order {
        ASC, DESC
    }
}
