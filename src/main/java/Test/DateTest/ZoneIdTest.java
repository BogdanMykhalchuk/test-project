package Test.DateTest;

import java.time.*;
import java.util.*;

public class ZoneIdTest {
    public static void main(String[] args) {
        Set<String> availableZoneIds = ZoneId.getAvailableZoneIds();
        availableZoneIds.forEach(System.out::println);
        System.out.println(availableZoneIds.size());

        ZoneId zoneId = ZoneId.of("America/Toronto");

        System.out.println(zoneId.getId());
        System.out.println(zoneId);

        LocalDateTime now = LocalDateTime.now();

        String offSet = now
                .atZone(zoneId)
                .getOffset()
                .getId()
                .replace("Z", "+00:00");

        System.out.println(String.format("(%s) %s", offSet, zoneId));
    }
}
