package Test.DateTest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.Locale;

import static java.time.format.DateTimeFormatter.ofPattern;

public class DateRangeTest {
    public static void main(String[] args) {
//        Test.String str = "2017-09-10 00:00";
        String string1 = "2017-09-29";
        String string2 = "2017-10-03";
        String string3 = "2017-10-10";
        String string4 = "2017-10-17";
        String string5 = "2017-10-30";

        String startRange = "2017-09-28";
        String endRange =  "2017-11-02";
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDateTime localDateTime = LocalDateTime.parse(str, formatter);

        LocalDate localDate1 = LocalDate.parse(string1, formatter1);
        LocalDate localDate2 = LocalDate.parse(string2, formatter1);
        LocalDate localDate3 = LocalDate.parse(string3, formatter1);
        LocalDate localDate4 = LocalDate.parse(string4, formatter1);
        LocalDate localDate5 = LocalDate.parse(string5, formatter1);

        LocalDate localDateStart = LocalDate.parse(startRange, formatter1);
        LocalDate localDateEnd = LocalDate.parse(endRange, formatter1);

        DateRangeTest.TopRepsFilter filter = new DateRangeTest.TopRepsFilter(localDateStart, localDateEnd);

//        System.out.println((localDateTime));
//        System.out.println(weekStartDate(localDateTime));
//        System.out.println(weekEndDate(localDateTime));
        System.out.println(String.format("Start of the week is - %s   End of the week is - %s", weekStartDate(localDate1, Locale.getDefault(), filter), weekEndDate(localDate1, Locale.getDefault(), filter)));
        System.out.println(String.format("Start of the week is - %s   End of the week is - %s", weekStartDate(localDate2, Locale.getDefault(), filter), weekEndDate(localDate2, Locale.getDefault(), filter)));
        System.out.println(String.format("Start of the week is - %s   End of the week is - %s", weekStartDate(localDate3, Locale.getDefault(), filter), weekEndDate(localDate3, Locale.getDefault(), filter)));
        System.out.println(String.format("Start of the week is - %s   End of the week is - %s", weekStartDate(localDate4, Locale.getDefault(), filter), weekEndDate(localDate4, Locale.getDefault(), filter)));
        System.out.println(String.format("Start of the week is - %s   End of the week is - %s", weekStartDate(localDate5, Locale.getDefault(), filter), weekEndDate(localDate5, Locale.getDefault(), filter)));

    }


    public static String weekEndDate(LocalDate date, Locale locale, DateRangeTest.TopRepsFilter filter) {
        return endOfWeek(date, locale).toString();
//        LocalDate lastDayOfRange = filter.getEndDate();
//        return endDate.isAfter(lastDayOfRange) ? lastDayOfRange.toString() : endDate.toString();
    }

    public static String weekStartDate(LocalDate date, Locale locale, DateRangeTest.TopRepsFilter filter) {
        return beginningOfWeek(date, locale).toString();
//        LocalDate firstDayOfRange = filter.getStartDate();
//        return startDate.isBefore(firstDayOfRange) ? firstDayOfRange.toString() : startDate.toString();
    }


    public static String weekRange(LocalDate date, Locale locale) {
        LocalDate startDate = beginningOfWeek(date, locale);
        LocalDate endDate = endOfWeek(date, locale);
        boolean isSameYear = startDate.getYear() == endDate.getYear();
        boolean isSameMonth = startDate.getMonth().equals(endDate.getMonth());
        String beginningFormat = isSameYear ? "MMM d" : "MMM d, yyyy";
        String endingFormat = isSameMonth && isSameYear ? "d, yyyy" : "MMM d, yyyy";
        return String.format("%s - %s", startDate.format(ofPattern(beginningFormat)), endDate.format(ofPattern(endingFormat)));
    }


    public static LocalDate beginningOfWeek(final LocalDate src, Locale locale) {
        return src.with(getDayOfWeek(locale), 1);
    }

    private static TemporalField getDayOfWeek(Locale locale) {
        return WeekFields.of(locale).dayOfWeek();
    }

    public static LocalDate endOfWeek(final LocalDate src, Locale locale) {
        return src.with(getDayOfWeek(locale), 7);
    }

    public static class TopRepsFilter {

        LocalDate startDate;
        LocalDate endDate;

        public TopRepsFilter() {
        }

        public TopRepsFilter(LocalDate startDate, LocalDate endDate) {
            this.startDate = startDate;
            this.endDate = endDate;
        }

        public LocalDate getStartDate() {
            return startDate;
        }

        public void setStartDate(LocalDate startDate) {
            this.startDate = startDate;
        }

        public LocalDate getEndDate() {
            return endDate;
        }

        public void setEndDate(LocalDate endDate) {
            this.endDate = endDate;
        }
    }
}
