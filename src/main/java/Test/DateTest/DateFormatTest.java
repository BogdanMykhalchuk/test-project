package Test.DateTest;

import java.time.*;
import java.time.format.*;

public class DateFormatTest {
    public static void main(String[] args) {
        DateTimeFormatter dateTimeFormatter1 = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        System.out.println(dateTimeFormatter1.format(LocalDate.now()));

        DateTimeFormatter dateTimeFormatter4 = DateTimeFormatter.ofPattern("dd/MM/YYYY");
        System.out.println(dateTimeFormatter4.format(LocalDate.now()));
    }
}
