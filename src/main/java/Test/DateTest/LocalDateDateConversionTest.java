package Test.DateTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class LocalDateDateConversionTest {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println("LocalDate is: " + localDate);
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("LocalDateTime is: " + localDateTime);
        Date dateFromLocalDate  = java.sql.Date.valueOf(localDate);
        System.out.println("Date from LocalDate is: " + dateFromLocalDate);
        Date dateFromLocalDateTime  = java.sql.Timestamp.valueOf(localDateTime);
        System.out.println("Date from LocalDateTime is: " + dateFromLocalDateTime);
    }
}
