package Test;

public class TestTask {
    public static void main(String[] args) {

        int[] array1 = {1};
        int[] array2 = {1, 1};
        int[] array3 = {1, 2, 3};
        int[] array4 = {3, 2, 1};
        int[] array5 = {2, 1, 2};
        int[] array6 = {1, 3, 2};
//        System.out.println(findMaxAndNext(new int[0]));
//        System.out.println(findMaxAndNext(array1));
//        System.out.println(findMaxAndNext(array2));
//        System.out.println(findMaxAndNext(array3));
//        System.out.println(findMaxAndNext(array4));
//        System.out.println(findMaxAndNext(array5));

//        System.out.println(findMaxAndNextByValue(new int[0]));
//        System.out.println(findMaxAndNextByValue(array1));
//        System.out.println(findMaxAndNextByValue(array2));
//        System.out.println(findMaxAndNextByValue(array3));
//        System.out.println(findMaxAndNextByValue(array4));
//        System.out.println(findMaxAndNextByValue(array5));

        System.out.println(findMaxAndNextInOneLoop(new int[0]));
        System.out.println(findMaxAndNextInOneLoop(array1));
        System.out.println(findMaxAndNextInOneLoop(array2));
        System.out.println(findMaxAndNextInOneLoop(array3));
        System.out.println(findMaxAndNextInOneLoop(array4));
        System.out.println(findMaxAndNextInOneLoop(array5));
        System.out.println(findMaxAndNextInOneLoop(array6));

    }

    private static String findMaxAndNextByValue(int[] array) {

        String value = "max: %s, next: %s";
        if (array.length == 0) {
            return String.format(value, 0, 0);
        }

        if (array.length == 1) {
            return String.format(value, array[0], 0);
        }

        int max = 0;
        int index = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                index = i;
            }
        }

        if (index + 1 < array.length) {
            return String.format(value, max, array[index + 1]);
        } else {
            return String.format(value, max, 0);
        }
    }

    private static String findMaxAndNext(int[] array) {

        String value = "max: %s, next: %s";
        if (array.length == 0) {
            return String.format(value, 0, 0);
        }

        if (array.length == 1) {
            return String.format(value, array[0], 0);
        }

        int max = 0;
        int secondMax = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }

            for (int j = 0; j < array.length; j++) {
                if (array[j] > secondMax && array[j] < max) {
                    secondMax = array[j];
                }
            }
        }

        return String.format(value, max, secondMax);
    }

    private static String findMaxAndNextInOneLoop(int[] array) {

        String value = "max: %s, next: %s";
        if (array.length == 0) {
            return String.format(value, 0, 0);
        }

        if (array.length == 1) {
            return String.format(value, array[0], 0);
        }

        if (array.length == 2) {
            if (array[0] > array[1]) {
                return String.format(value, array[0], array[1]);
            } else if (array[0] < array[1]) {
                return String.format(value, array[1], array[0]);
            } else {
                return String.format(value, array[0], 0);
            }
        }

        int max = 0;
        int secondMax = 0;


        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
            if (i > 0) {
                if (secondMax < array[i -1] && max > array[i -1]) {
                    secondMax = array[i -1];
                }
            }

            if (i == array.length - 1) {
                if (array[i] > secondMax && array[i] < max) {
                    secondMax = array[i];
                }
            }
        }

        return String.format(value, max, secondMax);
    }
}
