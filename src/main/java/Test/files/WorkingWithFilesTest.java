package Test.files;

import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class WorkingWithFilesTest {

    private final static String FIXTURES_CLASSPATH_BASE_PATH = "/Test/files/arza";
    public static void main(String[] args) throws IOException {

        File folder = new ClassPathResource(FIXTURES_CLASSPATH_BASE_PATH).getFile();

        for (File file : Objects.requireNonNull(folder.listFiles())) {
            System.out.println(file.getName().substring(0, file.getName().length() - 5));
        }
    }
}
