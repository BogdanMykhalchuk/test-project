package Test.CipherTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class CipherTest {
    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        Cipher cipher = null;
        try {
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
        }

        SecretKey secretKey = new SecretKeySpec("M02cnQ51Ji97vwT4".getBytes(), "AES");

        DataSourceProperties dataSorceProperties = new DataSourceProperties();
        dataSorceProperties.setDataSourceName("TEST_H2_1");
        dataSorceProperties.setUserName("username_1");
        dataSorceProperties.setPassword("password_1");
        dataSorceProperties.setUrl("jdbc:h2:mem:foo;DB_CLOSE_ON_EXIT=FALSE");
        dataSorceProperties.setDriverClassName("org.h2.Driver");
        dataSorceProperties.setDataSourceType(DataSourceType.H2);

        String string = null;
        try {
            string = objectMapper.writeValueAsString(dataSorceProperties);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        String encryptedString = encrypt(string, cipher, secretKey);
        System.out.println(encryptedString);
        String decryptedString = decrypt(encryptedString, cipher, secretKey);

        DataSourceProperties ds = null;
        try {
            ds = objectMapper.readValue(decryptedString, DataSourceProperties.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(ds.getDataSourceName());
        System.out.println(ds.getDataSourceType());
        System.out.println(ds.getDriverClassName());
        System.out.println(ds.getPassword());
        System.out.println(ds.getUrl());
        System.out.println(ds.getUserName());
    }

    private static String encrypt(String data, Cipher cipher, SecretKey secretKey) {

        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        // Encrypt the data
        try {
            byte[] encryptedBytes = cipher.doFinal(data.getBytes());
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    private static String decrypt(String data, Cipher cipher, SecretKey secretKey) {

        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

        // Decrypt the data
        try {
            byte[] decodedVal = Base64.getDecoder().decode(data);
            return new String(cipher.doFinal(decodedVal));
        } catch (BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
