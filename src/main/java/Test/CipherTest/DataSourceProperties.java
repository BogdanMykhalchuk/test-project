package Test.CipherTest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataSourceProperties {
    private String dataSourceName;
    private String userName;
    private String password;
    private String url;
    private String driverClassName;
    private DataSourceType dataSourceType;
}

