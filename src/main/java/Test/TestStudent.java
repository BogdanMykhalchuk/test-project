package Test;

import lombok.*;

import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TestStudent {
    private String name;
    private Integer age;

    public static void main(String[] args) {

        List<TestStudent> students = new ArrayList<>();

        TestStudent student1 = new TestStudent("Vasia", 23);
        students.add(student1);
        TestStudent student2 = new TestStudent("Petia", 24);
        students.add(student2);
        TestStudent student3 = new TestStudent("Vova", 22);
        students.add(student3);
        TestStudent student4 = new TestStudent("Vasia", 23);
        student1 = new TestStudent("Grisha", 5);

        // Try this with and without hashcode and equals
        System.out.println(students.contains(student1));
        System.out.println(students.contains(student2));
        System.out.println(students.contains(student3));
        System.out.println(students.contains(student4));
    }
}
