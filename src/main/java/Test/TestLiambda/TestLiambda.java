package Test.TestLiambda;

import java.time.LocalDate;
import java.util.regex.Pattern;

public class TestLiambda {
    private static final String FILE_NAME_PATTERN = "(^(.*)%s\\.csv$)";
    public static void main(String[] args) {
        String fileName = "958_fjl_A_2018-02-02.csv";
        boolean b = Pattern.matches(String.format(FILE_NAME_PATTERN, LocalDate.now()), fileName);
        System.out.println(b);
        String date = LocalDate.now().toString();
        String[] newFileName = fileName.split(date);
        String firstPart = newFileName[0];
        firstPart = firstPart.replaceAll("[^\\p{Alpha}^\\p{Digit}]", " ");
        System.out.println(firstPart);

    }
}
