package Test.JSON;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class Student implements Serializable {
    @JsonProperty("Name")
    private String name;

    @JsonProperty("Age")
    private Integer age;
}
