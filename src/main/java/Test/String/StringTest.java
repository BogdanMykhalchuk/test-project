package Test.String;

import java.util.UUID;

public class StringTest {
    public static void main(String[] args) throws Exception {

        String string = "Test.Test-@ '!?:\"{]25*()~`School!1@|/^&+=";
        System.out.println(createPortalName(string));
        System.out.println(string);

        String testString = " Кирилиця ";
        System.out.println(testString);
        System.out.println(testString.trim());

        System.out.println(String.format("Test.Test concatenation with %s", null));
    }

    private static String createPortalName(String schoolName) {
        String uuid = UUID.randomUUID().toString();
        String regexp = "[-,\\ !:;?#№._/@0-9$%&\"*\\(\\)\\{\\}\\[\\]=+\\^\\~\\`\\'\\|]";
        uuid = uuid.replace("-", "");
        schoolName = schoolName.replaceAll(regexp, "");
        schoolName = String.format("/%s/%s", uuid, schoolName);
        return schoolName;
    }
}
