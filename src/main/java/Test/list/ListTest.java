package Test.list;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ListTest {
    public static void main(String[] args) {
        // Initializing a Test.list of type Linkedlist
        List<Integer> l = new ArrayList<>();
        l.add(10);
        l.add(20);
        l.add(15);
        System.out.println(l);
        l.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i1.compareTo(i2);
            }
        });

        // Initializing another Test.list
        List<Integer> l2 = new ArrayList<Integer>();
        l2.add(10);
        l2.add(15);
        l2.add(20);
        System.out.println(l2);

        l2.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i1.compareTo(i2);
            }
        });

        if (l.equals(l2))
            System.out.println("Equal");
        else
            System.out.println("Not equal");
    }
}
