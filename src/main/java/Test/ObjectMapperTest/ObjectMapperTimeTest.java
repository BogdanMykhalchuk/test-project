package Test.ObjectMapperTest;

import Test.ObjectMapperTest.model.Address;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;

public class ObjectMapperTimeTest {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        String string = "2017-01-01T00:00:00";

        OffsetDateTime dateTime = objectMapper.readValue(string, OffsetDateTime.class);
        System.out.println(dateTime);

        Address address = new Address();
        address.setCity("New York");
        address.setHouseNumber("234");

        System.out.println(objectMapper.writeValueAsString(address));

        String date = "2018-10-23";
        System.out.println(objectMapper.readValue(date, LocalDate.class));
    }
}
