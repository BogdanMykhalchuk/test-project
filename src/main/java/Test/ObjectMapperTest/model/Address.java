package Test.ObjectMapperTest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Address {
    @JsonProperty("house")
    private String houseNumber;

    @JsonProperty("town")
    private String city;
}
