package Test.ObjectMapperTest.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Student {
    @JsonProperty("name")
    private String firstName;

    @JsonProperty("surName")
    private String lastName;

    @JsonProperty("address")
    private Address address;

    @JsonProperty("marks")
    private List<Mark> markList = new ArrayList<>(0);
}
