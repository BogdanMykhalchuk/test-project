package Test.ObjectMapperTest.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Mark {
    @JsonProperty("subject")
    private String subjectName;

    @JsonProperty("value")
    private String value;
}
