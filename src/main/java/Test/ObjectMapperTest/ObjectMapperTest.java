package Test.ObjectMapperTest;

import Test.ObjectMapperTest.model.Student;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ObjectMapperTest {
    public static void main(String[] args) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        Student student = null;
        try(FileInputStream fileInputStream = new FileInputStream("/home/bogdan/Java/Test.Test/src/main/resources/student.json")) {
            student = objectMapper.readValue(fileInputStream, Student.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(student);

        List<String> stringList = Arrays.asList("timmy", "garry", "tommy");
        System.out.println(objectMapper.writeValueAsString(stringList));
    }
}
