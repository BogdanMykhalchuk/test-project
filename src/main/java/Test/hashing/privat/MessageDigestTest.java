package Test.hashing.privat;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static Test.hashing.privat.Hash.convertToString;

public class MessageDigestTest {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        String data = "<oper>cmt</oper>" +
                "                    <wait>0</wait>" +
                "                    <test>0</test>" +
                "                    <payment id=\"1234567\">" +
                "                        <prop name=\"b_card_or_acc\" value=\"4627081718568608\" />" +
                "                        <prop name=\"amt\" value=\"1\" />" +
                "                        <prop name=\"ccy\" value=\"UAH\" />" +
                "                        <prop name=\"details\" value=\"test%20merch%20not%20active\" />";
        String password = "3A90E5J0f6OUIfqN1Qu59gYrjDgDblfL";
        String signatureString = data + password;
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] signatureMd5 = md5.digest(signatureString.getBytes());
        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
        byte[] signatureSha1 = sha1.digest(signatureMd5);
        String signature = convertToString(signatureSha1);

        System.out.println(signature);
    }
}
