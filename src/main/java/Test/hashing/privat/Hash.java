package Test.hashing.privat;

/**
 * {@code Hash} is a utility class that performs Test.hashing operations using
 * SHA-1 algorithm.
 * */
public class Hash {
    public static String convertToString(byte[] bytes) {
        StringBuilder builder = new StringBuilder();
        for (byte b : bytes) {
            builder.append(String.format("%02X", b));
        }
        return builder.toString();
    }
}
