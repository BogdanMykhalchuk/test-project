package Test.TestInterfaces;

public interface InterfaceWithDefoultMethod {
    default String getString() {
        return "Interface's method";
    }
}
