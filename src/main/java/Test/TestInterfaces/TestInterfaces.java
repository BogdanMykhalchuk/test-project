package Test.TestInterfaces;

public class TestInterfaces {
    public static void main(String[] args) {
        ClassWithoutOverridenMethod classWithoutOverridenMethod = new ClassWithoutOverridenMethod();
        ClassWithOverridenMethod classWithOverridenMethod = new ClassWithOverridenMethod();
        System.out.println(classWithoutOverridenMethod.getString());
        System.out.println(classWithOverridenMethod.getString());
    }
}
