package Test.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;

@Getter
public enum Currency {
    AUD("Australian Dollar($)"),
    BRL("Brazilian Real(R$)"),
    CAD("Canadian Dollar(CA$)"),
    CHF("Swiss Franc(Fr.)"),
    CZK("Czech Koruna(Kč)"),
    DKK("Danish Krone(kr.)"),
    EUR("Euro(€)"),
    GBP("Pound Sterling(£)"),
    HKD("Hong Kong Dollar($)"),
    HUF("Hungarian Forint(Ft)"),
    ILS("Israeli New Shekel(₪)"),
    JPY("Japanese Yen(¥)"),
    MYR("Malaysian Ringgit(RM)"),
    MXN("Mexican Peso(Mex$)"),
    NOK("Norwegian Krone(kr)"),
    NZD("New Zealand Dollar($)"),
    PHP("Philippine Peso(₱)"),
    PKR("Pakistan Rupee(₨)"),
    PLN("Polish Zloty(zł)"),
    RUB("Russian Ruble(руб )"),
    SEK("Swedish Krona(kr)"),
    SGD("Singapore Dollar(S$)"),
    THB("Thai Baht(฿)"),
    TRY("Turkish Lira(TL)"),
    TWD("Taiwan New Dollar(NT$)"),
    USD("U.S. Dollar($)");

    private String value;

    Currency(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static Currency create(String value) {
        for(Currency e: Currency.values()) {
            if (e.value.equals(value)) {
                return e;
            }
        }

        throw new IllegalArgumentException("Invalid type of Test.enums.Currency!!!");
    }
}
