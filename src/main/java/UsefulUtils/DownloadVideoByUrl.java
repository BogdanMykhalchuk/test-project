package UsefulUtils;

import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Created by Dreawalker on 13.03.2018.
 */
public class DownloadVideoByUrl {
    private static final String location = "D://Test/";
    private static final String localFileName = "Simpsons_09_16.avi";
    private final Path rootLocation = Paths.get(location);
    private static final String url = "http://moonwalk.center/serial/8d62c25da6b7bd23a61ef718d63af2a9/iframe?season=09&episode=16&nocontrols=1";
    private static final String fileLocation = location + localFileName;

    public static void main(String[] args) throws Exception {
        String finalUrl = getFinalLocation(url);
        URLConnection conn;
        InputStream is;
        try(BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(location + localFileName));) {
            URL url;
            byte[] buf;
            int byteRead, byteWritten = 0;
            url = new URL(finalUrl);
            conn = url.openConnection();
            is = conn.getInputStream();
            buf = new byte[100024];
            while ((byteRead = is.read(buf)) != -1) {
                outStream.write(buf, 0, byteRead);
                byteWritten += byteRead;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        saveFile();
    }

    public void store(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            Files.copy(file.getInputStream(), this.rootLocation.resolve(filename),
                    StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    public class StorageException extends RuntimeException {

        public StorageException(String message) {
            super(message);
        }

        public StorageException(String message, Throwable cause) {
            super(message, cause);
        }
    }

    public static String getFinalLocation(String address) throws IOException{
        URL url = new URL(address);
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        int status = conn.getResponseCode();
        if (status != HttpURLConnection.HTTP_OK)
        {
            if (status == HttpURLConnection.HTTP_MOVED_TEMP
                    || status == HttpURLConnection.HTTP_MOVED_PERM
                    || status == HttpURLConnection.HTTP_SEE_OTHER)
            {
                String newLocation = conn.getHeaderField("Location");
                return getFinalLocation(newLocation);
            }
        }
        return address;
    }

    private static void saveFile() throws Exception{
        URL website = new URL(url);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream(fileLocation);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
    }
}
