package UsefulUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import static java.util.Objects.nonNull;

public class CueTimingRemover {
    private final static String FILE_PATH = "C:\\Users\\DreamWalker\\Downloads\\Subtitles\\Godzilla.King.of.the.Monsters.2019.BDRip.1080p.seleZen_track9_[eng].txt";
    private final static String FILE_OUTPUT_PATH = createOutputPath(FILE_PATH);
    public static void main(String[] args) throws IOException {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(FILE_PATH));
             BufferedWriter fileWriter = new BufferedWriter(new FileWriter(FILE_OUTPUT_PATH))) {
            String line;

            while (nonNull(line = fileReader.readLine())) {
                line = fileReader.readLine();
                while (nonNull(line = fileReader.readLine()) && !"".equals(line)) {
                    fileWriter.write(String.format("%s\r\n", line));
                }
            }
        }
    }

    private static String createOutputPath(String path) {
        String folder = path.substring(0, path.lastIndexOf("\\"));
        String fullFileName = new File(path).getName();
        String fileExtension = fullFileName.substring(fullFileName.lastIndexOf("."));
        String fileName = fullFileName.substring(0, fullFileName.lastIndexOf("."));

        return String.format("%s//%s_PROCESSED_%s", folder, fileName, fileExtension);
    }
}
