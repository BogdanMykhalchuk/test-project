package UsefulUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dreawalker on 22.02.2018.
 */
public class SpringDecryption {
    public static void main(String[] args) {
        try (BufferedReader encryptedDocReader = new BufferedReader(
                new FileReader("C:\\Users\\Dreawalker\\Downloads\\New\\01.txt"));
             BufferedWriter decryptedFileWriter = new BufferedWriter(
                     new FileWriter("C:\\Users\\Dreawalker\\Downloads\\New\\02.txt")
             )) {
            String encryptedLine = encryptedDocReader.readLine();
            String[] encryptedStrings;
            List<String> decryptedStrings = new ArrayList<>();
            StringBuilder stringBuffer = new StringBuilder();
            while(encryptedLine != null) {
                encryptedStrings = encryptedLine.split(" ");
                decryptedStrings = decryptLine(encryptedStrings);
                for(String s : decryptedStrings) {
                    stringBuffer.append(s).append(" ");
                }
                decryptedFileWriter.write(stringBuffer.toString());
                encryptedLine = encryptedDocReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> decryptLine(String[] encryptedArray) {
        List<String> decryptedArray = new ArrayList<>();
        for(String s : encryptedArray) {
            decryptedArray.add(encryptWordByDictionary(s));
        }
        return decryptedArray;
    }

    private static String encryptWordByDictionary(String stringToDecrypt) {
        String encryptedString = stringToDecrypt;
        List<String> list = new ArrayList<>();
        try (BufferedReader dictionaryReader = new BufferedReader(
                new FileReader("C:\\Users\\Dreawalker\\Downloads\\New\\words.txt"))) {
            String dictionaryLine = dictionaryReader.readLine();
            while (dictionaryLine != null) {
                if (dictionaryLine.length() == stringToDecrypt.length()) {
                    if (stringLettersCompareWithDectionaryLine(stringToDecrypt, dictionaryLine)) {
                        list.add(dictionaryLine);
                    }
                }
                dictionaryLine = dictionaryReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (list.size() == 0) {
            encryptedString = stringToDecrypt;
        } else if (list.size() == 1) {
            encryptedString = list.get(0);
        } else {
            StringBuilder stringBuilder = new StringBuilder("(");
            for(String s : list) {
                stringBuilder.append(s).append(", ");
            }
            stringBuilder.append(")");
            encryptedString = stringBuilder.toString();
        }
        return encryptedString;
    }

    private static boolean stringLettersCompareWithDectionaryLine(String stringToDecrypt, String dictionaryLine) {
        char[] charsToDecrypt = dictionaryLine.toCharArray();
        String temp = stringToDecrypt.toLowerCase();
        for (int i = 0; i < charsToDecrypt.length; i++) {
            if (temp.indexOf(charsToDecrypt[i]) == -1) {
                return false;
            } else {
                temp = temp.replaceFirst(charsToDecrypt[i] + "", "");
            }
        }
        return temp.length() == 0;
    }
}
