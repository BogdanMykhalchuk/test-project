package UsefulUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.nonNull;

/**
 * Created by Dreawalker on 23.03.2018.
 */
public class TaniaCsvFileDateConvert {
    private static final String inputFile = "D://Tanya/input.csv";
    private static final String outputFile = "D://Tanya/output.csv";

    private static List<String> data = new ArrayList<>();

    public static void main(String[] args) {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(inputFile)));
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new File(outputFile)))) {
            String line = bufferedReader.readLine();
            String day;
            String month;
            String year;
            String result;
            while(nonNull(line)) {
                day = line.substring(0, 2);
                month = line.substring(2, 5);
                year = line.substring(5);
                month = monthConvert(month);
                result = String.format("%s.%s.%s\n", day, month, year);
                bufferedWriter.write(result);
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String monthConvert(final String stringToConvert) {
        switch (stringToConvert) {
            case("JAN"): return "01";
            case("FEB"): return "02";
            case("MAR"): return "03";
            case("APR"): return "04";
            case("MAY"): return "05";
            case("JUN"): return "06";
            case("JUL"): return "07";
            case("AUG"): return "08";
            case("SEP"): return "09";
            case("OCT"): return "10";
            case("NOV"): return "11";
            case("DEC"): return "12";
            default: return "NaN";
        }
    }
}
