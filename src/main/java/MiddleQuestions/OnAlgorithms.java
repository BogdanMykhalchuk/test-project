package MiddleQuestions;

// Big O notation is a way to measure how well a
// computer algorithm scales as the amount of data
// involved increases. It is not always a measure
// of speed as you'll see below

// This is a rough overview of Big O and doesn't 
// cover topics such as asymptotic analysis, which
// covers comparing algorithms as data approaches
// infinity

// What we are defining is the part of the algorithm 
// that has the greatest effect. For example
// 45n^3 + 20n^2 + 19 = 84 (n=1)
// 45n^3 + 20n^2 + 19 = 459 (n=2) Does 19 matter?
// 45n^3 + 20n^2 + 19 = 47019 (n=10) 
// Does the 20n^2 matter if 45n^3 = 45,000?
// Has an O(n^3) Order of n-cubed

import java.util.Arrays;

public class OnAlgorithms {

    private int[] theArray;

    private int arraySize;

    private int itemsInArray = 0;

//    private int count = 0;

    static long startTime;

    static long endTime;

    OnAlgorithms(int size) {

        arraySize = size;

        theArray = new int[size];

    }

    public static void main(String[] args) {

        System.out.println("Logging results for different N values for O(log N)");

        System.out.println(Math.log(2));
        System.out.println(Math.log(100));
        System.out.println(Math.log(1000));
        System.out.println(Math.log(10000));
        System.out.println(Math.log(100000));
        System.out.println(Math.log(1000000));
        System.out.println(Math.log(1000000000));

        System.out.println("/n");
        System.out.println("Logging results for different N values for O(N log N)");

        System.out.println(2 * Math.log(2));
        System.out.println(100 * Math.log(100));
        System.out.println(1000 * Math.log(1000));
        System.out.println(10000 * Math.log(10000));
        System.out.println(100000 * Math.log(100000));
        System.out.println(1000000 * Math.log(1000000));
        System.out.println(1000000000 * Math.log(1000000000));

        // Creating test objects with different quantities of elements

        OnAlgorithms testAlgo0 = new OnAlgorithms(10);
        testAlgo0.generateRandomArray();

        OnAlgorithms testAlgo00 = new OnAlgorithms(11);
        testAlgo00.generateRandomArray();

        OnAlgorithms testAlgo000 = new OnAlgorithms(12);
        testAlgo000.generateRandomArray();

        OnAlgorithms testAlgo0000 = new OnAlgorithms(100);
        testAlgo0000.generateRandomArray();

        testAlgo0000.quickSort(0, testAlgo0000.itemsInArray);
//        System.out.println(testAlgo0000.count);


        OnAlgorithms testAlgo1 = new OnAlgorithms(30000);
        OnAlgorithms testAlgo2 = new OnAlgorithms(100000);
        OnAlgorithms testAlgo3 = new OnAlgorithms(400000);
        OnAlgorithms testAlgo4 = new OnAlgorithms(1000000);

        //Filling arrays of test objects with values
        startTime = System.currentTimeMillis();
        testAlgo1.generateRandomArray();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Adding of 30000 elements into array took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo2.generateRandomArray();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Adding of 100000 elements into array took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo3.generateRandomArray();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Adding of 400000 elements into array took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo4.generateRandomArray();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Adding of 1000000 elements into array took %d msec", endTime - startTime));
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");



		//O(N) Test
        System.out.println("Start Test O(N)");
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo1.linearSearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Linear search element in Array with size 30000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo2.linearSearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Linear search element in Array with size 100000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
		testAlgo3.linearSearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Linear search element in Array with size 400000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo4.linearSearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Linear search element in Array with size 1000000 took %d msec", endTime - startTime));
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");



        // O(N^2) Test
        System.out.println("Start Test O(N^2)");
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo1.bubbleSort();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Bubble sort of Array with size 30000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo2.bubbleSort();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Bubble sort of Array with size 100000 took %d msec", endTime - startTime));
        System.out.println("\n");

        Arrays.sort(testAlgo3.theArray);
        System.out.println("Bubble sort of Array with size 400000 would took too many msec");
        System.out.println("\n");

        Arrays.sort(testAlgo4.theArray);
        System.out.println("Bubble sort of Array with size 1000000 would took too many msec");
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");

		 // 0 (log N) Test
        System.out.println("Start Test O(log N)");
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo1.binarySearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Binary search in Array with size 30000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo2.binarySearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Binary search in Array with size 100000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo3.binarySearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Binary search in Array with size 400000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo4.binarySearchForValue(20);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Binary search in Array with size 1000000 took %d msec", endTime - startTime));
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");

        // O (n log n) Test

        System.out.println("Start Test O(N log N)");
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo1.quickSort(0, testAlgo1.itemsInArray);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Quick Sort of Array with size 30000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo2.quickSort(0, testAlgo2.itemsInArray);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Quick Sort of Array with size 100000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo3.quickSort(0, testAlgo3.itemsInArray);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Quick Sort of Array with size 400000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo4.quickSort(0, testAlgo4.itemsInArray);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Quick Sort of Array with size 1000000 took %d msec", endTime - startTime));
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");


        // O(N!) Test (factorial of N)
        System.out.println("Start Test O(N!)");
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo0.nFacRuntimeFunc(testAlgo0.arraySize);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Factorial function of 10 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo00.nFacRuntimeFunc(testAlgo00.arraySize);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Factorial function of 11 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo000.nFacRuntimeFunc(testAlgo000.arraySize);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Factorial function of 12 took %d msec", endTime - startTime));
        System.out.println("\n");




        //0(1) Test
        System.out.println("Start Test O(1)");
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo1.addItemToArray(10);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Add element in Array with size 30000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo2.addItemToArray(10);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Add element in Array with size 100000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo3.addItemToArray(10);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Add element in Array with size 400000 took %d msec", endTime - startTime));
        System.out.println("\n");

        startTime = System.currentTimeMillis();
        testAlgo4.addItemToArray(10);
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Add element in Array with size 100000 took %d msec", endTime - startTime));
        System.out.println("\n");
        System.out.println("\n");
        System.out.println("\n");

    }

    // O(N!) An algorithm that executes recursively
    // and will behave like factorial of N
    void nFacRuntimeFunc(int n) {
        for(int i=0; i<n; i++) {
            nFacRuntimeFunc(n-1);
        }
    }

    // O(1) An algorithm that executes in the same
    // time regardless of the amount of data
    // This code executes in the same amount of
    // time no matter how big the array is

    public void addItemToArray(int newItem) {

        theArray[itemsInArray++] = newItem;

    }

    // 0(N) An algorithm that's time to complete will
    // grow in direct proportion to the amount of data
    // The linear search is an example of this

    // To find all values that match what we
    // are searching for we will have to look in
    // exactly each item in the array

    // If we just wanted to find one match the Big O
    // is the same because it describes the worst
    // case scenario in which the whole array must
    // be searched

    public void linearSearchForValue(int value) {
        boolean valueInArray = false;
        String emptyString = "";
        for (int i = 0; i < arraySize; i++) {
            if (theArray[i] == value) {
                valueInArray = true;
                emptyString = emptyString + theArray[i];
            }
        }
        if (valueInArray) {
            System.out.println("Value was found");
        }
    }

    // O(N^2) Time to complete will be proportional to
    // the square of the amount of data (Bubble Sort)
    // Algorithms with more nested iterations will result
    // in O(N^3), O(N^4), etc. performance

    // Each pass through the outer loop (0)N requires us
    // to go through the entire list again so N multiplies
    // against itself or it is squared

    public void bubbleSort() {

        startTime = System.currentTimeMillis();

        for (int i = arraySize - 1; i > 1; i--) {

            for (int j = 0; j < i; j++) {

                if (theArray[j] > theArray[j + 1]) {

                    swapValues(j, j + 1);

                }
            }
        }

        endTime = System.currentTimeMillis();

        System.out.println("Bubble Sort Took " + (endTime - startTime));
    }

    // O (log N) Occurs when the data being used is decreased
    // by roughly 50% each time through the algorithm. The
    // Binary search is a perfect example of this.

    // Pretty fast because the log N increases at a dramatically
    // slower rate as N increases

    // O (log N) algorithms are very efficient because increasing
    // the amount of data has little effect at some point early
    // on because the amount of data is halved on each run through

    public void binarySearchForValue(int value) {

        startTime = System.currentTimeMillis();

        int lowIndex = 0;
        int highIndex = arraySize - 1;

        int timesThrough = 0;

        while (lowIndex <= highIndex) {

            int middleIndex = (highIndex + lowIndex) / 2;

            if (theArray[middleIndex] < value)
                lowIndex = middleIndex + 1;

            else if (theArray[middleIndex] > value)
                highIndex = middleIndex - 1;

            else {

                System.out.println("\nFound a Match for " + value
                        + " at Index " + middleIndex);

                lowIndex = highIndex + 1;

            }

            timesThrough++;

        }

        // This doesn't really show anything because
        // the algorithm is so fast

        endTime = System.currentTimeMillis();

        System.out.println("Binary Search Took " + (endTime - startTime));

        System.out.println("Times Through: " + timesThrough);

    }

    // O (n log n) Most sorts are at least O(N) because
    // every element must be looked at, at least once.
    // The bubble sort is O(N^2)
    // To figure out the number of comparisons we need
    // to make with the Quick Sort we first know that
    // it is comparing and moving values very
    // efficiently without shifting. That means values
    // are compared only once. So each comparison will
    // reduce the possible final sorted lists in half.
    // Comparisons = log n! (Factorial of n)
    // Comparisons = log n + log(n-1) + .... + log(1)
    // This evaluates to n log n
    // In other words: (log n!) = (n log n) = (log n + log(n - 1) + ... + log(1));

    public void quickSort(int left, int right) {

        if (right - left <= 0)
            return;

        else {

            int pivot = theArray[right];

            int pivotLocation = partitionArray(left, right, pivot);

//            System.out.println("Left = " + left);
//            System.out.println("Right = " + right);
//            System.out.println("PivotLocation = " + pivotLocation);

            quickSort(left, pivotLocation - 1);
            quickSort(pivotLocation + 1, right);

        }

    }

    public int partitionArray(int left, int right, int pivot) {

        int leftPointer = left - 1;
        int rightPointer = right;

        while (true) {

            while (theArray[++leftPointer] < pivot) {
//                count++;
            }

            while (rightPointer > 0 && theArray[--rightPointer] > pivot) {
//                count++;
            }

            if (leftPointer >= rightPointer) {

                break;

            } else {

                swapValues(leftPointer, rightPointer);

            }

        }

        swapValues(leftPointer, right);

        return leftPointer;

    }

    public void generateRandomArray() {

        for (int i = 0; i < arraySize; i++) {

            theArray[i] = (int) (Math.random() * 1000) + 10;

        }

        itemsInArray = arraySize - 1;

    }

    public void swapValues(int indexOne, int indexTwo) {

        int temp = theArray[indexOne];
        theArray[indexOne] = theArray[indexTwo];
        theArray[indexTwo] = temp;

    }

}

