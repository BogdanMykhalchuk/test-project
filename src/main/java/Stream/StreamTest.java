package Stream;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.groupingBy;

public class StreamTest {
    public static void main(String[] args) {
        List<Fruit> fruits = prepareListOfFruits();
        groupByTest(fruits);
    }

    private static List<Fruit> prepareListOfFruits() {
        List<Fruit> list = new ArrayList<>();
        list.add(new Fruit("Orange", 20, "code:001 Orange"));
        list.add(new Fruit("Orange", 25, "code:002 Orange"));
        list.add(new Fruit("Apple", 20, "code:001 Apple"));
        list.add(new Fruit("Plum", 20, "code:001 Plum"));
        list.add(new Fruit("Pear", 19, "code:001 Pear"));
        list.add(new Fruit("Grape", 43, "code:001 Grape"));
        list.add(new Fruit("Banana", 15, "code:003 Banana"));
        list.add(new Fruit("Apricot", 26, "code:003 Apricot"));
        list.add(new Fruit("Pineapple", 30, "code:001 Pineapple"));

        return list;
    }

    private static void groupByTest(List<Fruit> fruits) {
        Collection<List<Fruit>> groupedFruits = fruits.stream().collect(groupingBy(StreamTest::createKeyByCode)).values();
        for (List<Fruit> fruitList : groupedFruits) {
            System.out.println((ArrayList<Fruit>) fruitList);
        }
    }

    private static String createKeyByCode(Fruit fruit) {
        if (nonNull(fruit.getCode())) {
            return fruit.getCode().substring(0, fruit.getCode().indexOf(" "));
        }
        return null;
    }
}
