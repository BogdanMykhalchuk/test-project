package Stream;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Fruit {
    private String name;
    private Integer price;
    private String code;

    @Override
    public String toString() {
        return name;
    }
}
